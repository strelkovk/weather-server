# Home Weather Server

This application integrates information from temperature sensors and weather providers to be shown in home dashboard.  

There are following endpoints
- GET /sensor - List of all sensors 
- GET /sensor/{id} - Sensor information by id
- GET /sensor-data - Recent data from sensors 
- POST /sensor-data - Save data from ESP-01 sensors
- GET /weather - Actual weather in Malaga
- GET /weather/forecast - 10 days weather forecast for Malaga

## Install the Application

Checkout out project into folder 
```bash
git clone git@gitlab.com:strelkovk/weather-server.git
```
Change to the project folder and install composer dependencies
```bash
cd weather-server
composer install
```
Configure database and OpenWeatherMap access token 
```bash
cp .env.example .env
```
Change database settings for your needs

Set OpenWeatherMap token. You can get it by signing up here https://openweathermap.org/appid
```bash
vim .env
```
Run database migrations
```bash
./vendor/bin/phoenix migrate
```
Run server
```bash
composer start
```
After that, open `http://localhost:8080` in your browser.

Run this command in the application directory to run the test suite
```bash
composer test
```
