<?php

declare(strict_types=1);

use Slim\App;

return function (App $app) {
    //uncomment to enable sessions
    //$app->add(App\Application\Middleware\SessionMiddleware::class);
};
