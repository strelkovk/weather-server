<?php

declare(strict_types=1);

use App\Application\Settings\Settings;
use App\Application\Settings\SettingsInterface;
use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {

    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/../');
    $dotenv->safeLoad();

    // Global Settings Object
    $containerBuilder->addDefinitions([
        SettingsInterface::class => function () {
            return new Settings([
                'displayErrorDetails' => true, // Should be set to false in production
                'logError'            => false,
                'logErrorDetails'     => false,
                'logger' => [
                    'name' => 'slim-app',
                    'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                    'level' => Logger::DEBUG,
                ],
                'db' => [
                    'driver' => $_ENV['DB_DRIVER'] ?? 'mysql',
                    'host' => $_ENV['DB_HOST'] ?? 'localhost',
                    'port' => $_ENV['DB_PORT'] ?? 3306,
                    'database' => $_ENV['DB_NAME'] ?? 'database',
                    'username' => $_ENV['DB_USER'] ?? '',
                    'password' => $_ENV['DB_PASSWORD'] ?? '',
                    'charset'   =>  'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix'    =>  $_ENV['DB_PREFIX'] ?? 3306,
                ],
                'weather' => [
                    'baseUrl' => $_ENV['OPEN_WEATHER_BASE_URL'] ?? '',
                    'query' => [
                        'units' => 'metric',
                        'appid' => $_ENV['OPEN_WEATHER_APPID'] ?? '',
                        'lon' => $_ENV['OPEN_WEATHER_LON'] ?? 0.0,
                        'lat' => $_ENV['OPEN_WEATHER_LAT'] ?? 0.0,
                    ]
                ]
            ]);
        }
    ]);
};
