<?php

declare(strict_types=1);

use App\Application\Settings\SettingsInterface;
use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Illuminate\Database\Connectors\ConnectionFactory;
use Illuminate\Database\Connection;
use Illuminate\Container\Container as IlluminateContainer;
use App\Domain\Weather\WeatherService;


return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $container) {
            $settings = $container->get(SettingsInterface::class);

            $loggerSettings = $settings->get('logger');
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },

        Connection::class => function (ContainerInterface $container) {
            $factory = new ConnectionFactory(new IlluminateContainer());
            $settings = $container->get(SettingsInterface::class);

            $connection = $factory->make($settings->get('db'));

            // Disable the query log to prevent memory issues
            $connection->disableQueryLog();

            return $connection;
        },

        PDO::class => function (ContainerInterface $container) {
            return $container->get(Connection::class)->getPdo();
        },

        WeatherService::class => function (ContainerInterface $container) {
            $settings = $container->get(SettingsInterface::class);
            $weatherSettings = $settings->get('weather');

            $client = new \GuzzleHttp\Client(
                [
                    'decode_content' => true,
                    'base_uri' => $weatherSettings['baseUrl'],
                    'query' => $weatherSettings['query'],
                ]
            );

            return new \App\Infrastructure\Services\OpenWeatherMapService($client);
        },
    ]);
};
