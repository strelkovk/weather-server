<?php

declare(strict_types=1);

use App\Application\Actions\Weather\ForecastAction;
use App\Application\Actions\Weather\CurrentAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        return $response;
    });

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write(<<<EOD
<pre>
- GET /sensor - List of sensors 
- GET /sensor/{id} - Sensor information by id
- GET /sensor-data - Recent data from sensors 
- POST /sensor-data - Save data from ESP-01 sensors
- GET /weather - Actual weather in Malaga
- GET /weather/forecast - 10 days weather forecast for Malaga
</pre>
EOD
        );
        return $response;
    });

    $app->group('/sensor', function (Group $group) {
        $group->get('', \App\Application\Actions\Sensor\ListSensorAction::class);
        $group->get('/{id}', \App\Application\Actions\Sensor\ViewSensorAction::class);
    });

    $app->group('/sensor-data', function (Group $group) {
        $group->get('', \App\Application\Actions\Sensor\SensorDataAction::class);
        $group->post('', \App\Application\Actions\Sensor\SaveSensorDataAction::class);
    });

    $app->group('/weather', function (Group $group) {
        $group->get('', CurrentAction::class);
        $group->get('/forecast', ForecastAction::class);
    });

};
