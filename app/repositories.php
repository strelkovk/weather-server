<?php

declare(strict_types=1);

use App\Domain\Sensor\SensorRepository;
use App\Domain\SensorData\SensorDataRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {

    $containerBuilder->addDefinitions([
        SensorRepository::class => \DI\autowire(\App\Infrastructure\Persistence\Sensor\MemorySensorRepository::class),
        SensorDataRepository::class => \DI\autowire(\App\Infrastructure\Persistence\SensorData\DbSensorDataRepository::class),
    ]);
};
