<?php

declare(strict_types=1);

namespace App\Domain\Sensor;

use JsonSerializable;

class Sensor implements JsonSerializable
{
    public function __construct(
        private ?int   $id,
        private bool   $indoor,
        private string $name,
        private string $token,
        private float  $latitude,
        private float  $longitude,
    )
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIndoor(): bool
    {
        return $this->indoor;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function checkToken ($token) {
        //@todo make token validation
        return true;
    }

    #[\ReturnTypeWillChange]
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'indoor' => $this->indoor,
            'name' => $this->name,
            'token' => $this->token,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
        ];
    }
}
