<?php

declare(strict_types=1);

namespace App\Domain\SensorData;

use Illuminate\Support\Collection;

interface SensorDataRepository
{
    /**
     * @return SensorData[]
     */
    public function findAll(): array;

    /**
     * @param int $id
     * @return SensorData
     * @throws SensorNotFoundException
     */
    public function findById(int $id): SensorData;

    public function getLastData(int $sensorId): Collection;

    public function save(SensorData $sensorData);

}