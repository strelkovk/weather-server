<?php

declare(strict_types=1);

namespace App\Domain\SensorData;

use JsonSerializable;

class SensorData implements JsonSerializable
{
    public function __construct(
        private ?int        $id,
        private int         $sensor_id,
        private float       $temperature,
        private float       $humidity,
        private float       $voltage,
        private string|null $created_at = null,
    )
    {
        $this->created_at =  $this->created_at ?? date('Y-m-d H:i:s');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getSensorId(): int
    {
        return $this->sensor_id;
    }

    /**
     * @return float
     */
    public function getTemperature(): float
    {
        return $this->temperature;
    }

    /**
     * @return float
     */
    public function getHumidity(): float
    {
        return $this->humidity;
    }

    /**
     * @return float
     */
    public function getVoltage(): float
    {
        return $this->voltage;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }


    #[\ReturnTypeWillChange]
    public function jsonSerialize(): array
    {
        return [

            'id' => $this->id,
            'sensor_id' => $this->sensor_id,
            'temperature' => $this->temperature,
            'humidity' => $this->humidity,
            'voltage' => $this->voltage,
            'created_at' => $this->created_at,
        ];
    }
}