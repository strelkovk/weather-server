<?php

declare(strict_types=1);

namespace App\Domain\Weather;

interface WeatherService
{
    public function current(): array;

    public function forecast(): array;
}