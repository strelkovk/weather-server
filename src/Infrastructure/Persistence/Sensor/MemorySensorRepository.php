<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Sensor;

use App\Domain\Sensor\Sensor;
use App\Domain\Sensor\SensorRepository;
use App\Domain\Sensor\SensorNotFoundException;

class MemorySensorRepository implements SensorRepository
{
    private array $sensors;

    public function __construct($sensors = null)
    {
        $this->sensors = $sensors ?? [
                1 => new Sensor(1, true, 'Sensor 1', md5('salt_1'), 0, 0),
                2 => new Sensor(2, false,'Sendor 2 outdoor', md5('salt_2'), 0 ,0),
            ];
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {
        return array_values($this->sensors);
    }

    /**
     * {@inheritdoc}
     */
    public function findById(int $id): Sensor
    {
        if (!isset($this->sensors[$id])) {
            throw new SensorNotFoundException();
        }

        return $this->sensors[$id];
    }

}
