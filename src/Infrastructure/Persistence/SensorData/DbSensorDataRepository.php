<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\SensorData;

use App\Domain\SensorData\SensorData;
use App\Domain\SensorData\SensorDataRepository;
use Illuminate\Database\Connection;
use Illuminate\Support\Collection;

class DbSensorDataRepository implements SensorDataRepository
{
    public function __construct(
        private Connection $connection,
    )
    {
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function findAll(): array
    {
        return $this->connection->table('sensor_data')->get();
    }

    public function findById(int $id): SensorData
    {
        return $this->connection->table('sensor_data')->find($id);
    }

    public function getLastData(int $sensorId): Collection
    {
        $record = $this->connection->table('sensor_data')
           ->where(['sensor_id' => $sensorId])
            ->orderBy('created_at', 'desc')
            ->take(1)
            ->get();
        return $record;
    }

    public function save(SensorData $sensorData)
    {
        return $this->connection->table('sensor_data')->insert([
            'sensor_id' => $sensorData->getSensorId(),
            'temperature' => $sensorData->getTemperature(),
            'humidity' => $sensorData->getHumidity(),
            'voltage' => $sensorData->getVoltage(),
            'created_at' => $sensorData->getCreatedAt(),
        ]);
    }
}
