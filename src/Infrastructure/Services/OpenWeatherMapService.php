<?php

namespace App\Infrastructure\Services;
use GuzzleHttp\ClientInterface;


use App\Domain\Weather\WeatherService;

class OpenWeatherMapService implements WeatherService {
    public function __construct(
        private ClientInterface $client,
    )
    {
    }


    public function current(): array
    {
        $response = $this->client->request('get', 'weather');
        if ($response->getStatusCode() == 200) {
            $json = json_decode($response->getBody()->getContents());
            return [$json];
        }
        return [];
    }

    public function forecast(): array
    {
        $response = $this->client->request('get', 'forecast');
        if ($response->getStatusCode() == 200) {
            $json = json_decode($response->getBody()->getContents());
            return $json?->list ?? [];
        }
        return [];
    }


}