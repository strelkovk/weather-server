<?php

declare(strict_types=1);

namespace App\Application\Actions\Weather;

use App\Application\Actions\Action;
use App\Domain\Weather\WeatherService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;

class ForecastAction extends Action
{
    public function __construct(
        LoggerInterface $logger,
        protected WeatherService $weatherService,
    )
    {
        parent::__construct($logger);
    }

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        return $this->respondWithData($this->weatherService->forecast());
    }
}