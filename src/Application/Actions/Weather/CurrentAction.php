<?php

declare(strict_types=1);

namespace App\Application\Actions\Weather;

use Psr\Http\Message\ResponseInterface as Response;

class CurrentAction extends ForecastAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        return $this->respondWithData($this->weatherService->current());
    }
}