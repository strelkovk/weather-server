<?php

declare(strict_types=1);

namespace App\Application\Actions\Sensor;

use App\Application\Actions\Action;
use App\Domain\Sensor\SensorRepository;

abstract class SensorAction extends Action
{
    public function __construct(
        protected SensorRepository $sensorRepository)
    {}
}