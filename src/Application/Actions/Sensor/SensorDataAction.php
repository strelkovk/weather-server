<?php

declare(strict_types=1);

namespace App\Application\Actions\Sensor;

use App\Domain\Sensor\SensorRepository;
use App\Domain\SensorData\SensorDataRepository;
use JetBrains\PhpStorm\Pure;
use Psr\Http\Message\ResponseInterface as Response;

class SensorDataAction extends SensorAction
{
    #[Pure] public function __construct(
        protected SensorRepository     $sensorRepository,
        protected SensorDataRepository $sensorDataRepository,
    )
    {
        parent::__construct($sensorRepository);
    }

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        return $this->respondWithData([
            'indoor' => $this->sensorDataRepository->getLastData(1)[0] ?? [],
            'outdoor' => $this->sensorDataRepository->getLastData(2)[0] ?? [],
        ]);
    }
}