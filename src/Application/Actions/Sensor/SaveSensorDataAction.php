<?php

declare(strict_types=1);

namespace App\Application\Actions\Sensor;

use App\Domain\SensorData\SensorData;
use Psr\Http\Message\ResponseInterface as Response;

class SaveSensorDataAction extends SensorDataAction
{

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $post = $this->request->getParsedBody();
        //file_put_contents('/home/strelkov/weather-server/post', print_r($post, true));
        $sensorId = (int)$post['id'] ?? 0;
        $temperature = (float)$post['temperature'] ?? 0.0;
        $humidity = (float)$post['humidity'] ?? 0.0;
        $voltage = (float)$post['v'] ?? 0.0;
        $token = $post['token'] ?? '';
        $sensor = $this->sensorRepository->findById($sensorId);

        if ($sensor && $sensor->checkToken($token) && $temperature && $humidity && $voltage) {
            $sensorData = new SensorData(null, $sensorId, $temperature, $humidity, $voltage);
            $this->sensorDataRepository->save($sensorData);
        }

        return $this->respondWithData([$sensor]);
    }
}