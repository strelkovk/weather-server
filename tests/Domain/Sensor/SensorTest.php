<?php

declare(strict_types=1);

namespace Tests\Domain\Sensor;

use App\Domain\Sensor\Sensor;
use Tests\TestCase;

class SensorTest extends TestCase
{
    public function sensorProvider(): array
    {
        return [
            [1, true, 'some name', 'token 1', 10, -19],
            [2, false, 'good name',  'token 2', 1.2, 30.2],
            [3, true, 'sensor name',  'token 3', 0.99999, -1.00001],
        ];
    }

    /**
     * @dataProvider sensorProvider
     * @param int    $id
     * @param bool $indoor
     * @param string $name
     * @param string $token
     * @param float $latitude
     * @param float $longitude
     */
    public function testGetters(int $id, bool $indoor, string $name, string $token, float $latitude, float  $longitude)
    {
        $sensor = new Sensor($id, $indoor, $name, $token, $latitude, $longitude);

        $this->assertEquals($id, $sensor->getId());
        $this->assertEquals($indoor, $sensor->getIndoor());
        $this->assertEquals($name, $sensor->getName());
        $this->assertEquals($token, $sensor->getToken());
        $this->assertEquals($latitude, $sensor->getLatitude());
        $this->assertEquals($longitude, $sensor->getLongitude());
    }

    /**
     * @dataProvider sensorProvider
     * @param int    $id
     * @param bool $indoor
     * @param string $name
     * @param string $token
     * @param float $latitude
     * @param float $longitude
     */
    public function testJsonSerialize(int $id, bool $indoor, string $name, string $token, float $latitude, float  $longitude)
    {
        $sensor = new Sensor($id, $indoor, $name, $token, $latitude, $longitude);

        $expectedPayload = json_encode([
            'id' => $id,
            'indoor' => $indoor,
            'name' => $name,
            'token' => $token,
            'latitude' => $latitude,
            'longitude' => $longitude,
        ]);

        $this->assertEquals($expectedPayload, json_encode($sensor));
    }
}
