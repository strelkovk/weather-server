<?php

declare(strict_types=1);

namespace Tests\Infrastructure\Persistence\Sensor;

use App\Domain\Sensor\Sensor;
use App\Domain\Sensor\SensorNotFoundException;
use App\Infrastructure\Persistence\Sensor\MemorySensorRepository;
use Tests\TestCase;

class MemorySensorRepositoryTest extends TestCase
{
    public function testFindAll()
    {
        $sensor = new Sensor(1, true, 'name 1','token', 1.0, -2.0);
        $sensorRepository = new MemorySensorRepository([1 => $sensor]);
        $this->assertEquals([$sensor], $sensorRepository->findAll());
    }

    public function testFindAllSensorsByDefault()
    {
        $sensors = [
            1 => new Sensor(1, true, 'name 1','token', 1.0, -2.0),
            2 => new Sensor(1, false, 'name 2','token', 3.0, 12.0),
        ];

        $sensorRepository = new MemorySensorRepository($sensors);
        $this->assertEquals(array_values($sensors), $sensorRepository->findAll());
    }

    public function testFindById()
    {
        $sensor = new Sensor(1, true, 'name 1','token', 1.0, -2.0);

        $sensorRepository = new MemorySensorRepository([1 => $sensor]);
        $this->assertEquals($sensor, $sensorRepository->findById(1));
    }

    public function testFindSensorByIdThrowsNotFoundException()
    {
        $sensorRepository = new MemorySensorRepository([]);
        $this->expectException(SensorNotFoundException::class);
        $sensorRepository->findById(1);
    }
}
