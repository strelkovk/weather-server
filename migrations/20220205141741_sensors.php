<?php

declare(strict_types=1);

use Phoenix\Migration\AbstractMigration;

final class Sensors extends AbstractMigration
{
    protected function up(): void
    {
        $this->table('sensor_data')
            ->addColumn('sensor_id', 'integer')
            ->addColumn('temperature', 'float', ['decimals' => 1])
            ->addColumn('humidity', 'float', ['decimals' => 1])
            ->addColumn('voltage', 'float', ['decimals' => 2])
            ->addColumn('created_at', 'datetime')
            ->create();
    }

    protected function down(): void
    {
        $this->table('sensor_data')->drop();
    }
}
