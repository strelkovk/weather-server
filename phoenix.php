<?php
require __DIR__ . '/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/');
$dotenv->safeLoad();

return [
    'migration_dirs' => [
        'migrations' => __DIR__ . '/migrations',
    ],
    'environments' => [
        'production' => [
            'adapter' => $_ENV['DB_DRIVER'] ?? 'mysql',
            'host' => $_ENV['DB_HOST'] ?? 'localhost',
            'port' => $_ENV['DB_PORT'] ?? 3306,
            'db_name' => $_ENV['DB_NAME'] ?? 'database',
            'username' => $_ENV['DB_USER'] ?? '',
            'password' => $_ENV['DB_PASSWORD'] ?? '',
            'charset'   =>  'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
    ],
    'default_environment' => 'production',
    'log_table_name' => 'phoenix_log',
];